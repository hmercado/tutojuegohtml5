// objetos de canvas
var canvas = document.getElementById("game");
var ctx = canvas.getContext("2d");
//definir vars para imagenes
var fondo;
// definicion de funciones.
function loadMedia() {
    fondo = new Image();
    fondo.src = "space-metal.jpg";
    fondo.onload = function () {
        var intervalo = window.setInterval(frameLoop, 1000 / 55);
    }
}

function drawBackground() {
    ctx.drawImage(fondo, 0, 0);
}

function frameLoop() {
    drawBackground();
}

//Ejecución de funciones
loadMedia();