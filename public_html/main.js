// objetos de canvas
var canvas = document.getElementById("game");
var ctx = canvas.getContext("2d");
// crear objeto de la nave.
var nave = {
    x: 100,
    y: canvas.height - 100,
    width: 50,
    height: 50
};
//definir vars para imagenes
var fondo;
// definicion de funciones.
function loadMedia() {
    fondo = new Image();
    fondo.src = "space-metal.jpg";
    fondo.onload = function () {
        var intervalo = window.setInterval(frameLoop, 1000 / 55);
    };
}

function dibujarFondo() {
    ctx.drawImage(fondo, 0, 0);
}

function dibujarNave() {
    ctx.save(); // guarda la info actual del contexto. (como un checkpint
    ctx.fillStyle = "white";
    ctx.fillRect(nave.x, nave.y, nave.width, nave.height);
    ctx.restore();
}

function frameLoop() {
    dibujarFondo();
    dibujarNave();
}

//Ejecución de funciones
loadMedia();